#! /bin/bash

set -u

image_whitelist="\(ubuntu\|centos\|phusion\|selenium\|amazon\|repl\)"

dry_run=1
images_to_keep=10
if [[ $# -gt 0 && "$1" = "--actually-run" ]]; then
	dry_run=0

	if [[ $# -eq 2 ]]; then
		if [[ "$2" -eq "$2" ]]; then # is $2 a number?
			images_to_keep=$2
		else
			echo "Second argument should be a number!"
			exit 1
		fi
	fi
fi

lines_to_delete=$((images_to_keep + 1))

get_old_images() {
	# `grep -v` inverts the selection, so we keep whitelisted images
	# `sed 1,Nd` deletes the first N lines, so we keep the most recent images
	docker images --no-trunc | grep -v "$image_whitelist" | sed "1,${lines_to_delete}d"
}

get_old_containers() {
	# we have no use for old containers on Jenkins, so delete any that aren't running
	# `sed 1d` is to delete the column headers
	docker ps -a --no-trunc -f status=exited | sed 1d
}

if [[ $dry_run -eq 1 ]]; then
	echo Old containers:
	get_old_containers
	echo Old images:
	get_old_images

	echo
	echo "run \`$0 --actually-run [number of images to keep]\` to delete things"
else
	# `awk "{print $n}"` only prints the nth column
	container_ids=$(get_old_containers | awk "{print \$1}")
	image_ids=$(get_old_images | awk "{print \$3}")

	if [[ "$container_ids" != "" ]]; then
		docker rm $container_ids
	fi

	if [[ "$image_ids" != "" ]]; then
		docker rmi $image_ids
	fi
fi
